"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ava_1 = __importDefault(require("ava"));
const utils_1 = require("./utils");
const from = {
    a: 1,
    b: 2,
    c: 3
};
ava_1.default('check that object with null properties are skipped', t => {
    const to = {
        a: null,
        b: 'b',
        c: 'c'
    };
    const actual = utils_1.assignDefined(Object.assign({}, from), to);
    const expected = {
        a: 1,
        b: 'b',
        c: 'c'
    };
    t.deepEqual(actual, expected);
});
ava_1.default('check that basic object assign mechanisms work', t => {
    const to = {
        a: 'a',
        b: 'b',
        c: 'c'
    };
    const actual = utils_1.assignDefined(Object.assign({}, from), to);
    t.deepEqual(actual, to);
});
ava_1.default('check that object with undefined properties are skipped', t => {
    t.log(from);
    const to = {
        a: 'a',
        b: undefined,
        c: undefined
    };
    const actual = utils_1.assignDefined(Object.assign({}, from), to);
    const expected = {
        a: 'a',
        b: 2,
        c: 3
    };
    t.deepEqual(actual, expected);
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuc3BlYy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvdXRpbHMuc3BlYy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLDhDQUF1QjtBQUN2QixtQ0FBd0M7QUFFeEMsTUFBTSxJQUFJLEdBQUc7SUFDWCxDQUFDLEVBQUUsQ0FBQztJQUNKLENBQUMsRUFBRSxDQUFDO0lBQ0osQ0FBQyxFQUFFLENBQUM7Q0FDTCxDQUFDO0FBRUYsYUFBSSxDQUFDLG9EQUFvRCxFQUFFLENBQUMsQ0FBQyxFQUFFO0lBQzdELE1BQU0sRUFBRSxHQUFHO1FBQ1QsQ0FBQyxFQUFFLElBQUk7UUFDUCxDQUFDLEVBQUUsR0FBRztRQUNOLENBQUMsRUFBRSxHQUFHO0tBQ1AsQ0FBQztJQUNGLE1BQU0sTUFBTSxHQUFHLHFCQUFhLG1CQUFNLElBQUksR0FBSSxFQUFFLENBQUMsQ0FBQztJQUM5QyxNQUFNLFFBQVEsR0FBRztRQUNmLENBQUMsRUFBRSxDQUFDO1FBQ0osQ0FBQyxFQUFFLEdBQUc7UUFDTixDQUFDLEVBQUUsR0FBRztLQUNQLENBQUM7SUFDRixDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztBQUNoQyxDQUFDLENBQUMsQ0FBQztBQUVILGFBQUksQ0FBQyxnREFBZ0QsRUFBRSxDQUFDLENBQUMsRUFBRTtJQUN6RCxNQUFNLEVBQUUsR0FBRztRQUNULENBQUMsRUFBRSxHQUFHO1FBQ04sQ0FBQyxFQUFFLEdBQUc7UUFDTixDQUFDLEVBQUUsR0FBRztLQUNQLENBQUM7SUFDRixNQUFNLE1BQU0sR0FBRyxxQkFBYSxtQkFBTSxJQUFJLEdBQUksRUFBRSxDQUFDLENBQUM7SUFDOUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7QUFDMUIsQ0FBQyxDQUFDLENBQUM7QUFFSCxhQUFJLENBQUMseURBQXlELEVBQUUsQ0FBQyxDQUFDLEVBQUU7SUFDbEUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNaLE1BQU0sRUFBRSxHQUFHO1FBQ1QsQ0FBQyxFQUFFLEdBQUc7UUFDTixDQUFDLEVBQUUsU0FBUztRQUNaLENBQUMsRUFBRSxTQUFTO0tBQ2IsQ0FBQztJQUVGLE1BQU0sTUFBTSxHQUFHLHFCQUFhLG1CQUFNLElBQUksR0FBSSxFQUFFLENBQUMsQ0FBQztJQUM5QyxNQUFNLFFBQVEsR0FBRztRQUNmLENBQUMsRUFBRSxHQUFHO1FBQ04sQ0FBQyxFQUFFLENBQUM7UUFDSixDQUFDLEVBQUUsQ0FBQztLQUNMLENBQUM7SUFDRixDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztBQUNoQyxDQUFDLENBQUMsQ0FBQyJ9