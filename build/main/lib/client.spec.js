"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ava_1 = __importDefault(require("ava"));
const lodash_1 = require("lodash");
const sinon_1 = __importDefault(require("sinon"));
const client_1 = require("./client");
// Resets the sinon sandbox after each test
ava_1.default.afterEach(() => {
    sinon_1.default.restore();
});
// Creates a context object that can be used to replace any method
ava_1.default.beforeEach(t => {
    t.context = {
        f: sinon_1.default.fake()
    };
});
// Global constants
const BIN_PATH = '/Users';
ava_1.default('can create client with no config', t => {
    const { f } = t.context;
    const client = new client_1.Client({});
    sinon_1.default.replace(client, 'spawn', f);
    client.launch(BIN_PATH);
    const a = f.args[0];
    const path = a[0];
    const flags = a[1];
    // This should match daemon path passed into the function
    t.is(path, BIN_PATH);
    // Check that default flags are passed in
    const hasApiAddr = flags.includes('--api-addr=localhost:9980');
    const hasHostAddr = flags.includes('--host-addr=:9982');
    const hasRpcAddr = flags.includes('--rpc-addr=:9981');
    t.true(hasApiAddr);
    t.true(hasHostAddr);
    t.true(hasRpcAddr);
});
ava_1.default('can replace client config', t => {
    const { f } = t.context;
    const client = new client_1.Client({
        agent: 'custom-agent',
        apiAuthentication: true,
        apiAuthenticationPassword: 'foo',
        apiHost: '1.1.1.1',
        apiPort: 1337,
        dataDirectory: 'bar',
        hostPort: 1339,
        modules: {
            consensus: true,
            explorer: true,
            gateway: true,
            host: true,
            miner: false,
            renter: true,
            transactionPool: true,
            wallet: true
        },
        rpcPort: 1338
    });
    sinon_1.default.replace(client, 'spawn', f);
    client.launch(BIN_PATH);
    const a = f.args[0][1];
    t.is(a.length, 8);
    t.true(lodash_1.some(a, x => x.includes('--agent=custom-agent')));
    t.true(lodash_1.some(a, x => x.includes('--api-addr=1.1.1.1:1337')));
    t.true(lodash_1.some(a, x => x.includes('--authenticate-api=true')));
    t.true(lodash_1.some(a, x => x.includes('--host-addr=:1339')));
    // skipped module test because we have a seperate unit test for parseModules
    t.true(lodash_1.some(a, x => x.includes('--rpc-addr=:1338')));
    t.true(lodash_1.some(a, x => x.includes('--scprime-directory=bar')));
    t.true(lodash_1.some(a, x => x.includes('--temp-password=foo')));
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LnNwZWMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL2NsaWVudC5zcGVjLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsOENBQXVCO0FBQ3ZCLG1DQUE4QjtBQUM5QixrREFBMEI7QUFDMUIscUNBQWtDO0FBRWxDLDJDQUEyQztBQUMzQyxhQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRTtJQUNsQixlQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7QUFDbEIsQ0FBQyxDQUFDLENBQUM7QUFFSCxrRUFBa0U7QUFDbEUsYUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRTtJQUNsQixDQUFDLENBQUMsT0FBTyxHQUFHO1FBQ1YsQ0FBQyxFQUFFLGVBQUssQ0FBQyxJQUFJLEVBQUU7S0FDaEIsQ0FBQztBQUNKLENBQUMsQ0FBQyxDQUFDO0FBRUgsbUJBQW1CO0FBQ25CLE1BQU0sUUFBUSxHQUFHLFFBQVEsQ0FBQztBQUUxQixhQUFJLENBQUMsa0NBQWtDLEVBQUUsQ0FBQyxDQUFDLEVBQUU7SUFDM0MsTUFBTSxFQUFFLENBQUMsRUFBRSxHQUFRLENBQUMsQ0FBQyxPQUFPLENBQUM7SUFDN0IsTUFBTSxNQUFNLEdBQUcsSUFBSSxlQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDOUIsZUFBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ2xDLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDeEIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNwQixNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbEIsTUFBTSxLQUFLLEdBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdCLHlEQUF5RDtJQUN6RCxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztJQUVyQix5Q0FBeUM7SUFDekMsTUFBTSxVQUFVLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO0lBQy9ELE1BQU0sV0FBVyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUN4RCxNQUFNLFVBQVUsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDdEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNuQixDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3BCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDckIsQ0FBQyxDQUFDLENBQUM7QUFFSCxhQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQyxDQUFDLEVBQUU7SUFDcEMsTUFBTSxFQUFFLENBQUMsRUFBRSxHQUFRLENBQUMsQ0FBQyxPQUFPLENBQUM7SUFDN0IsTUFBTSxNQUFNLEdBQUcsSUFBSSxlQUFNLENBQUM7UUFDeEIsS0FBSyxFQUFFLGNBQWM7UUFDckIsaUJBQWlCLEVBQUUsSUFBSTtRQUN2Qix5QkFBeUIsRUFBRSxLQUFLO1FBQ2hDLE9BQU8sRUFBRSxTQUFTO1FBQ2xCLE9BQU8sRUFBRSxJQUFJO1FBQ2IsYUFBYSxFQUFFLEtBQUs7UUFDcEIsUUFBUSxFQUFFLElBQUk7UUFDZCxPQUFPLEVBQUU7WUFDUCxTQUFTLEVBQUUsSUFBSTtZQUNmLFFBQVEsRUFBRSxJQUFJO1lBQ2QsT0FBTyxFQUFFLElBQUk7WUFDYixJQUFJLEVBQUUsSUFBSTtZQUNWLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLElBQUk7WUFDWixlQUFlLEVBQUUsSUFBSTtZQUNyQixNQUFNLEVBQUUsSUFBSTtTQUNiO1FBQ0QsT0FBTyxFQUFFLElBQUk7S0FDZCxDQUFDLENBQUM7SUFDSCxlQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDbEMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN4QixNQUFNLENBQUMsR0FBYSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRWpDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNsQixDQUFDLENBQUMsSUFBSSxDQUFDLGFBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pELENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDNUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM1RCxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RELDRFQUE0RTtJQUM1RSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3JELENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDNUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUMxRCxDQUFDLENBQUMsQ0FBQyJ9